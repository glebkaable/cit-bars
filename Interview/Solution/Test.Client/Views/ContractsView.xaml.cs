﻿using System;
using System.Windows.Controls;
using Test.Client.Models;
using Test.Client.ViewModels;

namespace Test.Client.Views
{
    /// <summary>
    /// Логика взаимодействия для ContractsView.xaml
    /// </summary>
    public partial class ContractsView : UserControl
    {
        public ContractsView()
        {
            InitializeComponent();

            DataContext = new ContractsViewModel();
        }

        private void Contracts_OnRowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            // если изменение отменено
            if (e.EditAction != DataGridEditAction.Commit)
            {
                // выходим
                return;
            }

            // получаем договор
            var contract = (IClientContract)e.Row.Item;

            // устанавливаем новую дату изменения
            contract.Contract.Modified = DateTime.Now;
        }
    }
}
