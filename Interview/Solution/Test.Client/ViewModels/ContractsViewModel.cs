﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Test.Client.Models;
using Test.Client.TestServiceReference;

namespace Test.Client.ViewModels
{
    public class ContractsViewModel
    {
        public ObservableCollection<IClientContract> Contracts { get; }

        public ICommand AddCommand { get; }

        public ICommand DeleteCommand { get; }

        public ICommand SaveCommand { get; }

        public ICommand RefreshCommand { get; }

        private List<Contract> OriginalContracts { get; set; }

        public ContractsViewModel()
        {
            Contracts = new ObservableCollection<IClientContract>();
            AddCommand = new Command(Add);
            DeleteCommand = new Command(Delete);
            SaveCommand = new Command(Save);
            RefreshCommand = new Command(Refresh);

            // загружаем договоры
            LoadContracts();
        }

        /// <summary>
        /// Загрузка договоров.
        /// </summary>
        private void LoadContracts()
        {
            // создаем соединение со службой
            using (var serviceClient = new ServiceClient())
            {
                // получаем список договоров
                var contracts = serviceClient
                    .GetContracts()
                    .ToList();

                // устанавливаем список в OriginalContracts
                OriginalContracts = contracts;

                // чистим список Contracts
                Contracts.Clear();

                // добавляем договоры в Contracts
                contracts.ForEach(x => Contracts.Add(new ClientContract(x)));
            }
        }

        /// <summary>
        /// Добавление договора в список.
        /// </summary>
        private void Add(object obj)
        {
            // создаем новый договор
            var newContract = new Contract
            {
                ID = Guid.NewGuid(),
                Number = "Б/н",
                Date = DateTime.Now,
                Created = DateTime.Now,
                Modified = DateTime.Now
            };

            // добаляем его с список
            Contracts.Add(new ClientContract(newContract));
        }

        /// <summary>
        /// Удаление договора из списка.
        /// </summary>
        private void Delete(object obj)
        {
            // получаем список выделенных договоров
            var selectedContracts = (IList)obj;

            // пробегаемся по списку выделенных договоров
            for (var i = selectedContracts.Count - 1; i >= 0; i--)
            {
                // получаем договор
                var selectedContract = (IClientContract)selectedContracts[i];

                // удаляем его из списка
                Contracts.Remove(selectedContract);
            }
        }

        /// <summary>
        /// Сохранение списка договоров.
        /// </summary>
        private void Save(object obj)
        {
            // получаем список договоров
            var contracts = Contracts
                .Select(x => x.Contract)
                .ToList();

            // получаем список новых договоров
            var contractsToInsert = contracts
                .Except(OriginalContracts)
                .ToList();

            // получаем список удаленных договоров
            var contractsToDelete = OriginalContracts
                .Except(contracts)
                .ToList();

            // получаем список договоров для обновления
            var contractsToUpdate = contracts
                .Union(OriginalContracts)
                .ToList();

            // создаем соединение со службой
            using (var serviceClient = new ServiceClient())
            {
                // добалвяем договоры
                contractsToInsert.ForEach(x => 
                    serviceClient.InsertContract(x));

                // удалаяем договоры
                contractsToDelete.ForEach(x => 
                    serviceClient.DeleteContract(x.ID));

                // обновляем договоры
                contractsToUpdate.ForEach(x => 
                    serviceClient.UpdateContract(x));
            }

            // загружаем договоры
            LoadContracts();
        }

        /// <summary>
        /// Обновление списка договоров.
        /// </summary>
        private void Refresh(object obj)
        {
            // загружаем договоры
            LoadContracts();
        }
    }
}
