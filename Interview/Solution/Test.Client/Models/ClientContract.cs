﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Test.Client.Annotations;
using Test.Client.TestServiceReference;

namespace Test.Client.Models
{
    /// <summary>
    /// Договор (для клиента).
    /// </summary>
    public class ClientContract : IClientContract, INotifyPropertyChanged
    {
        /// <summary>
        /// Серверный договор.
        /// </summary>
        public Contract Contract { get; }

        /// <summary>
        /// Актуальность договора.
        /// </summary>
        public bool IsActual => Contract.Modified.AddDays(60).Date > DateTime.Now;

        /// <summary>
        /// Создает клиентский договор, на основе серверного.
        /// </summary>
        /// <param name="contract">Серверный договор.</param>
        public ClientContract(Contract contract)
        {
            // устанавливаем договор
            Contract = contract;

            // подписываемся на изменение свойств договора
            Contract.PropertyChanged += Contract_PropertyChanged;
        }

        /// <summary>
        /// Обработка события изменения свойства договора.
        /// </summary>
        private void Contract_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // если это дата изменения
            if (e.PropertyName == nameof(Contract.Modified))
            {
                // сообщаем, что свойство IsActual изменилось
                OnPropertyChanged(nameof(IsActual));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
