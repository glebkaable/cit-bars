﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Test.Client.TestServiceReference;

namespace Test.Client.Models
{
    [DataContract]
    public class ContractComparer : IEqualityComparer<Contract>
    {
        public int GetHashCode(Contract contract)
        {
            return contract.ID.GetHashCode();
        }

        public bool Equals(Contract contract1, Contract contract2)
        {
            if (contract1 == null ||
                contract2 == null)
            {
                return false;
            }

            return contract1.ID == contract2.ID;
        }
    }
}
