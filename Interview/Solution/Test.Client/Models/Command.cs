﻿using System;
using System.Windows.Input;

namespace Test.Client.Models
{
    public class Command : ICommand
    {
        private Action<object> Method { get; }

        public event EventHandler CanExecuteChanged;

        public Command(Action<object> method)
        {
            Method = method;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Method.Invoke(parameter);
        }
    }
}
