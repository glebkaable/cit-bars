﻿using Test.Client.TestServiceReference;

namespace Test.Client.Models
{
    public interface IClientContract
    {
        /// <summary>
        /// Серверный договор.
        /// </summary>
        Contract Contract { get; }

        /// <summary>
        /// Актуальность договора.
        /// </summary>
        bool IsActual { get; }
    }
}
