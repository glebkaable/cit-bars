﻿using System.ServiceProcess;

namespace Test.Server
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ServiceBase.Run(new WindowService.WindowService());
        }
    }
}
