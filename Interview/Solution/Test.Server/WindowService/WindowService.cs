﻿using System.ServiceModel;
using System.ServiceProcess;
using Test.Server.ServiceContract;

namespace Test.Server.WindowService
{
    public class WindowService : ServiceBase
    {
        public ServiceHost ServiceHost;

        public WindowService()
        {
            ServiceName = "TestService";
        }

        protected override void OnStart(string[] args)
        {
            ServiceHost?.Close();
            ServiceHost = new ServiceHost(typeof(Service));
            ServiceHost.Open();
        }

        protected override void OnStop()
        {
            ServiceHost?.Close();
            ServiceHost = null;
        }
    }
}
