﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Test.Server.WindowService
{
    [RunInstaller(true)]
    public class WindowServiceInstaller : Installer
    {
        public WindowServiceInstaller()
        {
            var processInstaller = new ServiceProcessInstaller
            {
                Account = ServiceAccount.LocalSystem
            };

            var serviceInstaller = new ServiceInstaller
            {
                ServiceName = "TestService"
            };

            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}
