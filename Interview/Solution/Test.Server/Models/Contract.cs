﻿using System;
using System.Runtime.Serialization;

namespace Test.Server.Models
{
    [DataContract]
    public class Contract : IContract
    {
        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public DateTime Modified { get; set; }
    }
}
