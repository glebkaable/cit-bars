﻿using System;

namespace Test.Server.Models
{
    public interface IContract
    {
        Guid ID { get; set; }

        string Number { get; set; }

        DateTime Date { get; set; }

        DateTime Created { get; set; }

        DateTime Modified { get; set; }
    }
}
