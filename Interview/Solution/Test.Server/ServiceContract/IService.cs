﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Test.Server.Models;

namespace Test.Server.ServiceContract
{
    [ServiceContract(Namespace = "http://TestService")]
    public interface IService
    {
        /// <summary>
        /// Получение списка договоров.
        /// </summary>
        [OperationContract]
        Task<List<Contract>> GetContractsAsync();

        /// <summary>
        /// Обновление договора.
        /// </summary>
        /// <param name="contract">Договор.</param>
        [OperationContract]
        Task UpdateContractAsync(Contract contract);

        /// <summary>
        /// Удаление договора.
        /// </summary>
        /// <param name="contractID">ID договора.</param>
        [OperationContract]
        Task DeleteContractAsync(Guid contractID);

        /// <summary>
        /// Добавление договора.
        /// </summary>
        /// <param name="contract">Договор.</param>
        [OperationContract]
        Task InsertContractAsync(Contract contract);
    }
}
