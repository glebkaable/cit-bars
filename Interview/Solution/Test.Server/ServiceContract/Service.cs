﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Test.Server.Models;

namespace Test.Server.ServiceContract
{
    public class Service : IService
    {
        private string ConnectionString { get; }

        public Service()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["TestService"].ConnectionString;
        }

        public async Task<List<Contract>> GetContractsAsync()
        {
            // задаем текст запроса
            const string commandText =
                @"SELECT
	                [ID]
	                , [Number]
	                , [Date]
	                , [Created]
	                , [Modified]
                FROM
	                [dbo].[Contracts] c WITH(NOLOCK)";

            // выполняем запрос
            return await ExecuteListAsync<Contract>(commandText);
        }

        public async Task UpdateContractAsync(Contract contract)
        {
            // задаем текст запроса
            const string commandText =
                @"UPDATE [dbo].[Contracts]
                SET
	                [Number] = @Number
	                , [Date] = @Date
	                , [Created] = @Created
	                , [Modified] = @Modified
                WHERE [ID] = @ID";

            // задаем параметры запроса
            var parameters = new[]
            {
                new SqlParameter("@ID", contract.ID),
                new SqlParameter("@Number", contract.Number),
                new SqlParameter("@Date", contract.Date),
                new SqlParameter("@Created", contract.Created),
                new SqlParameter("@Modified", contract.Modified),
            };

            // выполняем запрос
            await ExecuteNonQueryAsync(commandText, parameters);
        }

        public async Task DeleteContractAsync(Guid contractID)
        {
            // задаем текст запроса
            const string commandText =
                @"DELETE FROM [dbo].[Contracts]
                WHERE [ID] = @ID";

            // задаем параметры запроса
            var parameters = new[]
            {
                new SqlParameter("@ID", contractID)
            };

            // выполняем запрос
            await ExecuteNonQueryAsync(commandText, parameters);
        }

        public async Task InsertContractAsync(Contract contract)
        {
            // задаем текст запроса
            const string commandText =
                @"INSERT INTO [dbo].[Contracts]
                (
	                [ID]
	                , [Number]
	                , [Date]
	                , [Created]
	                , [Modified]
                )
                SELECT
	                @ID
	                , @Number
	                , @Date
	                , @Created
	                , @Modified";

            // задаем параметры запроса
            var parameters = new[]
            {
                new SqlParameter("@ID", contract.ID),
                new SqlParameter("@Number", contract.Number),
                new SqlParameter("@Date", contract.Date),
                new SqlParameter("@Created", contract.Created),
                new SqlParameter("@Modified", contract.Modified),
            };

            // выполняем
            await ExecuteNonQueryAsync(commandText, parameters);
        }

        /// <summary>
        /// Выполнение запроса.
        /// </summary>
        /// <param name="commandText">Текст запроса.</param>
        /// <param name="parameters">Параметры запроса.</param>
        private async Task ExecuteNonQueryAsync(
            string commandText,
            SqlParameter[] parameters = null)
        {
            // создаем соединение с БД
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                // открываем соединение
                sqlConnection.Open();

                // создаем запрос
                using (var command = sqlConnection.CreateCommand())
                {
                    // задаем текст запроса
                    command.CommandText = commandText;

                    // если параметры заданы
                    if (parameters != null)
                    {
                        // добавляем параметры в запрос
                        command.Parameters.AddRange(parameters);
                    }

                    // выполняем запрос
                    await command.ExecuteNonQueryAsync();
                }
            }
        }

        /// <summary>
        /// Выполнение запроса, возвращаемого список объектов указанного типа.
        /// </summary>
        /// <typeparam name="T">Тип объекта.</typeparam>
        /// <param name="commandText">Текст запроса.</param>
        /// <param name="parameters">Параметры запроса.</param>
        private async Task<List<T>> ExecuteListAsync<T>(
            string commandText,
            SqlParameter[] parameters = null)
            where T : new()
        {
            // объявляем список объектов указанного типа
            var objectsList = new List<T>();

            // получаем список свойств указанного типа
            var typeProperties = typeof(T).GetProperties();

            // создаем соединение с БД
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                // открываем соединение
                sqlConnection.Open();

                // создаем запрос
                using (var command = sqlConnection.CreateCommand())
                {
                    // устанавливаем текст запроса
                    command.CommandText = commandText;

                    // если параметры заданы
                    if (parameters != null)
                    {
                        // добавляем параметры в запрос
                        command.Parameters.AddRange(parameters);
                    }

                    // выполняем запрос и получаем объект для чтения данных
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        // пока данные читаются (бежим по строкам)
                        while (reader.Read())
                        {
                            // создаем объект указанного типа
                            var obj = new T();

                            // бежим по свойствам типа
                            foreach (var typeProperty in typeProperties)
                            {
                                // если свойство нельзя установить
                                if (!typeProperty.CanWrite)
                                {
                                    // смотрим дальше
                                    continue;
                                }

                                // получаем индекс колонки по наименованию свойства
                                var columnIndex = reader.GetOrdinal(typeProperty.Name);

                                // если такой колонки нет
                                if (columnIndex == -1)
                                {
                                    // смотрим дальше
                                    continue;
                                }

                                // получаем значение колонки по индексу
                                var columnValue = reader.GetValue(columnIndex);

                                // устанавливаем значение в объект
                                typeProperty.SetValue(obj, columnValue);
                            }

                            // добавляем объект в список
                            objectsList.Add(obj);
                        }
                    }
                }

                // возвращаем список объектов
                return objectsList;
            }
        }
    }
}
